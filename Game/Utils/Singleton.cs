﻿using System;

public class Singleton<T> where T : new()
{
    protected static T instance = default(T);

    protected Singleton()
    {
    }

    public static T Instance
    {
        get
        {
            return GetInstance();
        }
    }

    public static T GetInstance()
    {
        if (Singleton<T>.instance == null)
            Singleton<T>.instance = Activator.CreateInstance<T>();

        return Singleton<T>.instance;
    }
}