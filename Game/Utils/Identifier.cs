﻿/// <summary>
/// 同一个类型的不同实例设定具有独一无二的ID
/// 使用方法：继承此类即可
/// </summary>
/// <typeparam name="T">该表示的类型</typeparam>
public class Identifier<T>
{
    public Identifier()
    {
        identifierId = identifierLastId;
        ++identifierLastId;
    }
    public int GetId()
    {
        return identifierId;
    }

    private int identifierId;
    private static int identifierLastId = 0;
}