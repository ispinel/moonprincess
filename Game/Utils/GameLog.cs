﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public class GameLog
    {
        public static void Log(string text)
        {
#if UNITY_EDITOR
            string textFinal = string.Format("[Log] {0}", text);
            Debug.Log(textFinal);
#endif
        }

        public static void LogWarning(string text)
        {
            string textFinal = string.Format("[Warning!] {0}", text);
            Debug.LogWarning(textFinal);
        }

        public static void LogError(string text)
        {
            string textFinal = string.Format("[Error!] {0}", text);
            Debug.LogError(textFinal);
        }
    }
}