﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public class Main:MonoBehaviour
    {
        private Game game;
        void Awake()
        {
            if(game == null)
            {
                game = Game.Instance;
                game.Init();
            }
        }
        void Update()
        {
            if(game != null)
            {
                game.Update();
            }
        }
    }
}
