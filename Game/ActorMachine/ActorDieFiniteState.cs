﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class ActorDieFiniteState : FiniteState
    {
        private ActorMachine machine;
        public ActorDieFiniteState(ActorMachine machine)
        {
            this.machine = machine;
        }
        public override void Enter()
        {
            base.Enter();
        }

        public override void Update()
        {
            base.Update();
        }

        public override void Exit()
        {
            base.Exit();
        }
    }
}
