﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public class ActorMachine
    {
        private Actor actor;
        private Vector3 nextMove;
        private bool crouch;

        public enum State
        {
            WALK,
            IDLE,
            RUN,
            ATTACK,
            JUMP,
            DIE
        }

        private FSMachine<State> machine = new FSMachine<State>();
        private ActorAttackFiniteState attackState;
        private ActorDieFiniteState dieState;
        private ActorIdleFiniteState idleState;
        private ActorRunFiniteState runState;
        private ActorWalkFiniteState walkState;
        private ActorJumpFiniteState jumpState;

        public ActorMachine(Actor actor)
        {
            this.actor = actor;

            attackState = new ActorAttackFiniteState(this);
            dieState = new ActorDieFiniteState(this);
            idleState = new ActorIdleFiniteState(this);
            runState = new ActorRunFiniteState(this);
            walkState = new ActorWalkFiniteState(this);
            jumpState = new ActorJumpFiniteState(this);

            machine.AddState(State.ATTACK, attackState);
            machine.AddState(State.DIE, dieState);
            machine.AddState(State.IDLE, idleState);
            machine.AddState(State.JUMP, jumpState);
            machine.AddState(State.RUN, runState);
            machine.AddState(State.WALK, walkState);
        }

        public void Update()
        {
            machine.Update();
        }
        public void RunTo(Vector3 nextMove)
        {
            this.NextMove = nextMove;

            machine.SwitchToState(State.RUN);
        }
        public void WalkTo(Vector3 nextMove, bool crouch)
        {
            this.NextMove = nextMove;
            this.Crouch = crouch;

            machine.SwitchToState(State.WALK);
        }
        public void Idle()
        {
            machine.SwitchToState(State.IDLE);
        }
        public void Jump(Vector3 nextMove)
        {
            machine.SwitchToState(State.JUMP);
        }
        public void Attack()
        {
            machine.SwitchToState(State.ATTACK);
        }

        public FSMachine<State> FSMachine
        {
            get
            {
                return machine;
            }
        }

        public Vector3 NextMove
        {
            get
            {
                return nextMove;
            }
            set
            {
                nextMove = value;
            }
        }

        public bool Crouch
        {
            get
            {
                return crouch;
            }
            set
            {
                crouch = value;
            }
        }

        public Actor TargetActor
        {
            get
            {
                return actor;
            }
        }
    }
}
