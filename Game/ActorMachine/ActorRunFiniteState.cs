﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class ActorRunFiniteState : FiniteState
    {
        private ActorMachine machine;
        public ActorRunFiniteState(ActorMachine machine)
        {
            this.machine = machine;
        }

        public override void Enter()
        {
            base.Enter();
        }

        public override void Update()
        {
            base.Update();

            ActorAnimatorController controller = machine.TargetActor.GetComponent<ActorAnimatorController>();

            if (controller == null)
            {
                return;
            }

            controller.Move(machine.NextMove, machine.Crouch, false);   
        }

        public override void Exit()
        {
            base.Exit();
        }
    }
}
