﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class ActorWalkFiniteState : FiniteState
    {
        private ActorMachine machine;
        public ActorWalkFiniteState(ActorMachine machine)
        {
            this.machine = machine;
        }
        public override void Enter()
        {
            base.Enter();

//             ActorAnimatorController controller = machine.TargetActor.GetComponent<ActorAnimatorController>();
// 
//             if(controller == null)
//             {
//                 return;
//             }
// 
//             controller.Move(machine.NextMove, machine.Crouch, false);      
        }

        public override void Update()
        {
            base.Update();

            ActorAnimatorController controller = machine.TargetActor.GetComponent<ActorAnimatorController>();

            if (controller == null)
            {
                return;
            }

            controller.Move(machine.NextMove * 0.5f, machine.Crouch, false);      
        }

        public override void Exit()
        {
            base.Exit();
        }
    }
}
