﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class FSMachine<StateType>
    {
        private Dictionary<StateType, FiniteState> states = new Dictionary<StateType, FiniteState>();
        private FiniteState currentState;

        public void AddState(StateType id, FiniteState state)
        {
            if(state == null)
            {
                return;
            }

            states[id] = state;
        }
        public FiniteState GetState(StateType id)
        {
            FiniteState state = null;
            if (states.TryGetValue(id, out state))
            {
                return state;
            }

            return null;
        }
        public void Update()
        {
            if(currentState == null)
            {
                return;
            }

            currentState.Update();
        }

        public void SwitchToState(StateType id)
        {
            FiniteState nextState = GetState(id);

            if(nextState == null)
            {
                GameLog.LogError(string.Format("FSMachine.SwitchToState error,can not find {0} state",id));
                return;
            }

            if(nextState == currentState)
            {
                return;
            }

            if(currentState != null)
            {
                currentState.Exit();
            }

            nextState.Enter();
            currentState = nextState;
        }
        public FiniteState GetCurrentState()
        {
            return currentState;
        }

        public int GetCurrentStateId()
        {
            if(currentState == null)
            {
                return -1;
            }

            return currentState.Id;
        }
    }
}
