﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class FiniteState
    {
        private int id;

        public virtual void Update()
        {

        }
        public virtual void Enter()
        {

        }

        public virtual void Exit()
        {

        }

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
    }
}
