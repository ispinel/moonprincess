﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    /// <summary>
    /// 游戏章节管理器
    /// </summary>
    public class ChapterManager:Singleton<ChapterManager>
    {
        private Dictionary<int, Chapter> chapters = new Dictionary<int, Chapter>();

        public void AddChapter(Chapter chapter)
        {
            if(chapter == null)
            {
                return;
            }

            if(chapters.ContainsKey(chapter.Id))
            {
                return;
            }

            chapters.Add(chapter.Id, chapter);
        }
        public Chapter GetChapter(int id)
        {
            Chapter chapter = null;
            if(chapters.TryGetValue(id, out chapter))
            {
                return chapter;
            }

            return null;
        }

        public int CurrentChapterId
        {
            get;
            set;
        }

        public Chapter GetCurrentChapter()
        {
            return GetChapter(CurrentChapterId);
        }
    }
}