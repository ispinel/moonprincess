﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public class Chapter
    {
        public enum Type
        {
            EMPTY,
            PARKOUR,
        }

        private readonly Type type = Type.EMPTY;
        private readonly int id = 0;
        private Vector3 majorActorBornPos;

        public Chapter(Type type,int id)
        {
            this.type = type;
            this.id = id;
        }

        public Type ChapterType
        {
            get
            {
                return type;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }
        }

        public Vector3 MajorActorBornPos
        {
            get
            {
                return majorActorBornPos;
            }
            set
            {
                majorActorBornPos = value;
            }
        }
    }
}