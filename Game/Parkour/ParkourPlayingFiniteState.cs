﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class ParkourPlayingFiniteState : FiniteState
    {
        private ParkourChapterPlayer player;
        public ParkourPlayingFiniteState(ParkourChapterPlayer player)
        {
            this.player = player;
        }

        public override void Enter()
        {
            base.Enter();
        }

        public override void Update()
        {
            base.Update();
        }

        public override void Exit()
        {
            base.Exit();
        }
    }
}