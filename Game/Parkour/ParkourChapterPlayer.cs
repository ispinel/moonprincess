﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class ParkourChapterPlayer
    {
        public enum State
        {
            DIALOGUE,
            PLAYING
        }

        private FSMachine<State> machine = new FSMachine<State>();
        private Chapter chapter;
        private ParkourPlayingFiniteState playingState;
        private ParkourDialogueFiniteState dialogueState;

        public ParkourChapterPlayer()
        {
            playingState = new ParkourPlayingFiniteState(this);
            dialogueState = new ParkourDialogueFiniteState(this);

            machine.AddState(State.DIALOGUE, dialogueState);
            machine.AddState(State.PLAYING, playingState);
        }

        public void Play(Chapter chapter)
        {
            this.chapter = chapter;

            // based chapter data,spawn marjor actor,monsters,etc...

            MajorActor majorActor = ActorManager.Instance.SpawnMajorActor(0) as MajorActor;

            majorActor.transform.position = chapter.MajorActorBornPos;

            ActorManager.Instance.SpawnActor(0);
        }
        public void PlayDialog()
        {
            machine.SwitchToState(State.DIALOGUE);
        }
        public void PlayGame()
        {
            machine.SwitchToState(State.PLAYING);
        }

        public void Update()
        {
            machine.Update();
        }
    }
}