﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class ParkourDialogueFiniteState : FiniteState
    {
        private ParkourChapterPlayer player;
        private int dialogueId;

        public ParkourDialogueFiniteState(ParkourChapterPlayer player)
        {
            this.player = player;
        }

        public int DialogueId
        {
            get
            {
                return dialogueId;
            }
            set
            {
                dialogueId = value;
            }
        }

        public override void Update()
        {
            base.Update();
            player.PlayGame();
        }
    }
}