﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class Game:Singleton<Game>
    {
        private FSMachine<GameState> machine = new FSMachine<GameState>();
        private ActorManager actorManager;

        public enum GameState
        {
            INIT = 0,
            MAINUI,
            PLAYPARKOUR,
            EXIT
        }

        public void Init()
        {
            actorManager = ActorManager.Instance;

            machine.AddState(GameState.INIT, new GameInitFiniteState());
            machine.AddState(GameState.MAINUI, new MainUIFiniteState());
            machine.AddState(GameState.PLAYPARKOUR, new PlayParkourFiniteState());
            machine.AddState(GameState.EXIT, new GameExitFiniteState());

            machine.SwitchToState((int)GameState.INIT);
        }

        public void Update()
        {
            machine.Update();
            actorManager.Update();
        }

        public void Exit()
        {

        }
        public FSMachine<GameState> FSMachine
        {
            get
            {
                return machine;
            }
        }
    }
}
