﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public class ActorInputController
    {
        private Vector3 move;
        private bool jump;

        public void Update(Actor targetActor)
        {
            if(targetActor == null)
            {
                return;
            }

            if (!jump)
            {
                jump = UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager.GetButtonDown("Jump");
            }

            float horizontal = UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager.GetAxis("Horizontal");
            //float v = UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager.GetAxis("Vertical");
            bool crouch = Input.GetKey(KeyCode.C);

            move = horizontal * Vector3.right;

            bool run = true;
#if !MOBILE_INPUT
            // walk speed multiplier
            if (Input.GetKey(KeyCode.LeftShift))
            {
                run = true;
                //move *= 0.5f;
            }     
#endif

//             if(move == Vector3.zero)
//             {
//                 targetActor.ActorMachine.Idle();
//             }
//             else if(jump == true)
//             {
//                 targetActor.ActorMachine.Jump(move);
//             }
//             else
//             {
//                 if(run)
//                 {
//                     targetActor.ActorMachine.RunTo(move);
//                 }
//                 else
//                 {
//                     targetActor.ActorMachine.WalkTo(move, crouch);
//                 }
//             }

            ActorAnimatorController controller = targetActor.GetComponent<ActorAnimatorController>();

            controller.Move(move, crouch, false);

            jump = false;
        }
    }
}