﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public abstract class Actor : MonoBehaviour
    {
        private ActorMachine machine = null;

        public enum Type
        {
            EMPTY,
            MAJORACTOR,
            TREEMONSTER,
        }

        public Actor(Type type)
        {
            this.type = type;
            machine = new ActorMachine(this);
        }

        private class ActorId:Identifier<ActorId>
        {}

        private ActorId actorId = new ActorId();

        private Type type;

        private ActorAnimatorController animatorController;

        /// <summary>
        /// Actor对应的数据配置ID
        /// </summary>
        private int ActorDataId
        {
            get;
            set;
        }

        public int GetId()
        {
            return actorId.GetId();
        }
        public int Id
        {
            get
            {
                return actorId.GetId();
            }
        }

        public void Update()
        {
            machine.Update();
        }

        public static Actor Create(Type type)
        {
            if(type == Type.TREEMONSTER)
            {
                return new TreeMonster();
            }

            if(type == Type.MAJORACTOR)
            {
                return new MajorActor();
            }

            return null;
        }

        private void Start()
        {
            animatorController = GetComponent<ActorAnimatorController>();
        }

        public ActorAnimatorController AnimatorController
        {
            get
            {
                return animatorController;
            }
        }

        public ActorMachine ActorMachine
        {
            get
            {
                return machine;
            }
        }
    }
}
