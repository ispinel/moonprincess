﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public class TreeMonster : Actor
    {
        [SerializeField]
        public float speed = 1.0f;

        private BoxCollider collider;
        private bool isRunning = true;

        public TreeMonster()
            :base(Type.TREEMONSTER)
        {
    
        }

        void Awake()
        {
            if(ActorManager.Instance.IsContainTreeMonster(this.Id) == false)
            {
                ActorManager.Instance.AddTreeMonster(this);
            }

            collider = transform.GetComponent<BoxCollider>();

            isRunning = true;
        }

        void Update()
        {
            if (isRunning == false)
            {
                return;
            }

            Vector3 pos = transform.position;

            pos.x = pos.x - speed * Time.deltaTime;

            transform.position = pos;
        }

        void OnCollisionEnter()
        {
            isRunning = false;
        }
    }
}
