﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public class ActorManager:Singleton<ActorManager>
    {
        private Dictionary<int, TreeMonster> treeMonsters = new Dictionary<int, TreeMonster>();

        private MajorActor majorActor;
        private ActorInputController inputController;

        public ActorManager()
        {
            inputController = new ActorInputController();
        }

        public MajorActor MajorActor
        {
            get
            {
                return majorActor;
            }
        }

        public void AddTreeMonster(TreeMonster monster)
        {
            if(monster == null)
            {
                return;
            }

            if(treeMonsters.ContainsKey(monster.GetId()))
            {
                treeMonsters.Remove(monster.Id);
            }

            treeMonsters.Add(monster.Id, monster);
        }

        private TreeMonster SpawnTreeMonster(string prefab)
        {
            TreeMonster treeMonster = Actor.Create(Actor.Type.TREEMONSTER) as TreeMonster;

            AddTreeMonster(treeMonster);

            return treeMonster;
        }

        /// <summary>
        /// 产生一个Actor
        /// </summary>
        /// <param name="dataId">Actor需要的数据表ID</param>
        /// <returns></returns>
        public Actor SpawnActor(int dataId)
        {
            // Get actor data info...

            Actor.Type type = Actor.Type.TREEMONSTER;
            string prefab = "Actors/Prefabs/Tree";

            Object prefabObject = Resources.Load(prefab);

            if(prefabObject == null)
            {
                return null;
            }

            GameObject targetObject = GameObject.Instantiate(prefabObject) as GameObject;

            if (targetObject == null)
            {
                return null;
            }

            Actor result = null;

            if(type == Actor.Type.TREEMONSTER)
            {
                result = targetObject.AddComponent<TreeMonster>();
                AddTreeMonster(result as TreeMonster);
            }

            return result;
        }

        /// <summary>
        /// Temp Test
        /// </summary>
        /// <param name="dataId"></param>
        /// <returns></returns>
        public Actor SpawnMajorActor(int dataId)
        {
            Actor.Type type = Actor.Type.MAJORACTOR;
            string prefab = "Actors/Prefabs/Ethan";

            Object prefabObject = Resources.Load(prefab);

            if (prefabObject == null)
            {
                return null;
            }

            GameObject targetObject = GameObject.Instantiate(prefabObject) as GameObject;

            if (type == Actor.Type.MAJORACTOR)
            {
                majorActor = targetObject.AddComponent<MajorActor>();
            }

            return majorActor;
        }

        public bool IsContainTreeMonster(int id)
        {
            return treeMonsters.ContainsKey(id);
        }

        public void Update()
        {
            inputController.Update(MajorActor);
        }
    }
}
