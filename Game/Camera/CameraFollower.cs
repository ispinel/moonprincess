﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public class CameraFollower : MonoBehaviour
    {
        [SerializeField]
        private GameObject target = null;

        private Vector3 target_pos;
        void Update()
        {
            if (target != null)
            {
                target_pos = gameObject.transform.position;
                gameObject.transform.position = new Vector3(target.transform.position.x, target_pos.y, target_pos.z);
            }
        }

        public GameObject Target
        {
            set
            {
                target = value;
            }
            get
            {
                return target;
            }
        }
    }
}
