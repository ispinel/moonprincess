﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public class CameraShake : MonoBehaviour
    {
        [SerializeField]
        private float startTime = 0.0f;
        [SerializeField]
        private float shakeLength = 0.0f;
        [SerializeField]
        private float shakeStrength = 0.0f;
        [SerializeField]
        private bool vibrate = false;
        [SerializeField]
        private float vibrateStartTime = 0.0f;
        [SerializeField]
        private float vibrateLength = 0.0f;

        private Vector3 deltaPos = Vector3.zero;
        private float currentTime;
        private bool isVibrated = false;
        private float lastVibrateTime = 0.0f;
        private bool isWork = true;

        private static Vector3 resetPos = Vector3.one;
        /// <summary>
        /// 用来复位被震动后的摄像机
        /// 为什么是static?当多个震动一起工作的时候，这个初始值获取是被震动后的值，因此需要获取最前面的那个震动的值
        /// 相当于共享变量
        /// </summary>
        private static Vector3 originalPos = resetPos;
        //运行计数
        private static int runningCount = 0;

        private bool isResetedPos = false;
        private bool isInitedOriginalPos = false;

        public void Init()
        {
            currentTime = 0.0f;
            isVibrated = false;
            lastVibrateTime = 0.0f;
            //originalPos = Vector3.one;
            isResetedPos = false;
            isInitedOriginalPos = false;

            if (runningCount >= 2)
            {
                isWork = false;
            }
            else
            {
                isWork = true;
            }
        }
        void Start()
        {
            //Init();
        }

        void Awake()
        {
            Init();
        }
        void OnEnable()
        {
            //Init();
        }

        void OnDestroy()
        {
            if (isWork == false)
            {
                return;
            }

            if (isResetedPos != true)
            {
                if (runningCount > 0)
                {
                    --runningCount;
                }

                if (originalPos != resetPos && runningCount <= 0)
                {
                    if (Camera.main != null)
                    {
                        originalPos.x = Camera.main.transform.localPosition.x;
                        Camera.main.transform.localPosition = originalPos;
                    }

                    originalPos = resetPos;
                }

                isResetedPos = true;
            }
        }
        public static Vector3 OriginalPos
        {
            get
            {
                return originalPos;
            }
        }

        public static void ResetOriginalPos()
        {
            originalPos = resetPos;
            runningCount = 0;
        }

        void Update()
        {
            if (isWork == false)
            {
                return;
            }

            currentTime += Time.deltaTime;

            if (Camera.main == null)
            {
                return;
            }

            int hash = this.GetHashCode();

            if (currentTime > startTime && currentTime <= (startTime + shakeLength))
            {
                if (isInitedOriginalPos == false)
                {
                    if (originalPos == resetPos && runningCount <= 0)
                    {
                        originalPos = Camera.main.transform.localPosition;
                    }

                    isInitedOriginalPos = true;
                    ++runningCount;
                }

                Camera.main.transform.localPosition -= deltaPos;
                deltaPos = UnityEngine.Random.insideUnitSphere / shakeStrength;
                Camera.main.transform.localPosition += deltaPos;
            }

            if (currentTime >= (startTime + shakeLength))
            {
                if (isResetedPos != true)
                {
                    if (runningCount > 0)
                    {
                        --runningCount;
                    }

                    if (originalPos != resetPos && runningCount <= 0)
                    {
                        originalPos.x = Camera.main.transform.localPosition.x;
                        Camera.main.transform.localPosition = originalPos;
                        originalPos = resetPos;
                    }

                    isResetedPos = true;
                }
            }

            if (vibrate == false)
            {
                return;
            }

            // 使用Handheld.Vibrate();接口震动手机，只能固定0.5s，使用这些设定来延长震动
            if (isVibrated && (vibrateStartTime + vibrateLength) - currentTime >= 0.5f)
            {
                if (lastVibrateTime == 0.0f || (currentTime - lastVibrateTime) >= 0.5f)
                {
                    lastVibrateTime = currentTime;
#if UNITY_ANDROID || UNITY_IPHONE
                Handheld.Vibrate();
#endif
                }
            }

            if (currentTime > vibrateStartTime && isVibrated == false)
            {
                lastVibrateTime = currentTime;
#if UNITY_ANDROID || UNITY_IPHONE
            Handheld.Vibrate();
#endif
                isVibrated = true;
            }
        }
    }
}
