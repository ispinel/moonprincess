﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Game
{
    public class CameraManager : Singleton<CameraManager>
    {
        public void EnableCameraShake(bool enable)
        {
            CameraShake shake = GetCameraShake();

            if(shake != null)
            {
                shake.enabled = shake;
            }
        }

        public CameraShake GetCameraShake()
        {
            Camera camera = GetCamera();

            return camera.gameObject.AddComponent<CameraShake>();
        }

        public CameraFollower GetCameraFollower()
        {
            Camera camera = GetCamera();

            return camera.gameObject.AddComponent<CameraFollower>();
        }

        public Camera GetCamera()
        {
            return Camera.main;
        }

        public void InitCameraEffect()
        {
            Camera camera = GetCamera();

            if(camera.GetComponent<CameraFollower>())
            {
                camera.gameObject.AddComponent<CameraFollower>();
            }

            if(camera.GetComponent<CameraShake>())
            {
                CameraShake shake = camera.gameObject.AddComponent<CameraShake>();
                shake.enabled = false;
            }
        }
    }
}