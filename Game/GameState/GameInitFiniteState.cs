﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class GameInitFiniteState : FiniteState
    {
        public override void Enter()
        {
            base.Enter();

            FileManager.Instance.LoadFiles();
        }

        public override void Update()
        {
            base.Update();
            Game.Instance.FSMachine.SwitchToState(Game.GameState.MAINUI);
        }
    }
}