﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class MainUIFiniteState : FiniteState
    {
        public override void Update()
        {
            base.Update();
            Game.Instance.FSMachine.SwitchToState(Game.GameState.PLAYPARKOUR);
        }
    }
}