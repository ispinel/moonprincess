﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Game
{
    public class PlayParkourFiniteState : FiniteState
    {
        private ParkourChapterPlayer player = new ParkourChapterPlayer();

        public override void Enter()
        {
            base.Enter();

            Chapter chapter = ChapterManager.Instance.GetCurrentChapter();
            if(chapter == null)
            {
                return;
            }

            player.Play(chapter);

            CameraManager.Instance.InitCameraEffect();

            CameraFollower follower = CameraManager.Instance.GetCameraFollower();
            follower.Target = ActorManager.Instance.MajorActor.gameObject;
        }

        public override void Update()
        {
            base.Update();

            player.Update();
        }
    }
}