﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Game
{
    public class FileManager:Singleton<FileManager>
    {
        public Dictionary<FileParser, string> parsers = new Dictionary<FileParser, string>();

        public void Init()
        {
            parsers.Add(new ChapterFileParser(), "Config/chapters.json");
            parsers.Add(new GlobalFileParser(), "Config/global.json");
        }
        public void LoadFiles()
        {
            foreach(var itr in parsers)
            {
                itr.Key.Init(itr.Value);
                itr.Key.Parse();
            }
        }
    }
}