﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Game
{
    public class ChapterFileParser:FileParser
    {
        public override void Parse()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.ProhibitDtd = false;
            XmlReader reader = XmlReader.Create(file, settings);

            reader.ReadToFollowing("root");

            while(reader.Read())
            {
                //reader.
            }

            Chapter chapter = new Chapter(Chapter.Type.PARKOUR, 1);

            ChapterManager.Instance.AddChapter(chapter);
            ChapterManager.Instance.CurrentChapterId = 1;
        }
    }
}